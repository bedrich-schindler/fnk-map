# FNK mapa

## Testování

1. Nainstalování BrowserSync přes NPM: `npm i browser-sync`
2. Spuštění BrowserSync: `browser-sync start --server --files "."` (běží na portu 3000 a lze se na něj připojit z libovolného zařízení v místní síti)

## Nastavení API:
```
window.map = {
    apiUrl: 'http://forumnovakarolina.dev.smarcoms.cz/cs',
    apiShopListPath: '/mapa',
    apiShopDetailPath: '/mapa?shopid=:id',
    apiCategoryListPath: '/mapa-seznam',
    apiCategoryDetailPath: '/mapa-kategorie/:id',
}
```

## Transformace dat

Ve `window.map` existují funkce `transformShopData` a `transformCategoryData`, které transformují příchozí data z Drupal API do vnitřní struktury aplikace:
```
transformShopData: function (shop) {
  return {
    id: shop.nid[0].value,
  mapNumber: shop.field_cislo_obchodu.map(function (mapNumber) {
      return mapNumber.value;
  }),
  name: shop.title [0].value,
  level: shop.field_podlazi.map(function (level) {
      return level.value;
  }),
  openingHours: shop.field_oteviraci_doba[0].value,
  };
},

transformCategoryData: function (category) {
  return {
    id: category.tid[0].value,
  name: category.name[0].value
  };
},
```

## Náležitosti SVG

```
<!-- Original SVG - nezměněno -->
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="svg1" width="1583.5" height="808.65" enable-background="new 0 0 2000 2000" version="1.1" viewBox="0 0 1583.5 808.65" xml:space="preserve">

	<!-- SVG View: Tento prvek je transformován (jsou na něj navázány funkce zoom a pan) - přidáno -->
	<g id="svg1-view">

		<!-- Clickable SVG: Na tento prvek jsou navázány události pro zoom a pan, aby bylo možné posouvat obraz i při kliknutí mimo polygon - přidáno -->
		<svg>

			...

			<!-- Shop group - Skupina sdružující polygony a další skupiny obchodu a obsahuje taktéž text s názvem obchodu - nezměněno -->
			<g id="shop123" class="shop">

				<!-- pogylony, skupiny, čtverce ... - nezměněno ->
				<polygon></polygon>
				<g>
					<polygon></polygon>
				 </g>

				<!-- Shop label - Text element bez textu, do kterého bude pomocí JS dohrán název obchodu - přidáno ->
				<text x="1260" y="935" id="shop123-label" class="shop-label"></text>

			</g>

			...

		<svg>

	</g>

</svg>
```

* Original SVG - Normální `svg` element s unikátním `id` (např. `svgLevel0`).
* SVG View  - Element `g`, který musí mít `id` stejné jako Original SVG, s postfixem `-view` (např. `svgLevel0`)
* Clickable SVG - `svg` element bez žádné vlastnosti
* Shop group - `g` element s unikátním `id` napříč mapama ve formátu `shop.....` (musí začínat vždy slovem `shop`, např. `shop1`, `shopKFC`, ...)
* Shop label - `text` element bez názvu obchodu (načte se přes JS), který musí mít `id` stejné jako Shop group, s postfixem `-label`  (např. `shop1-label`, `shopKFC-label`)

## Poznámky

Projekt vznikl vykopírováním HTML, CSS a JS ze stránky s mapou na FNK. Zároveň jsem odstanil jsem několikset řádků JavaScriptu, který není již aktuální a není potřeba, neboť jsme se rozhodli pro podporu IE10+. CSS jsem řešil minimálně a všechny CSS jsou CSS vykopírované z původního projektu, takže při fron-end revizi je možné jej promazat.
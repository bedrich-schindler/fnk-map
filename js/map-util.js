if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(obj, start) {
		for (var i = (start || 0), j = this.length; i < j; i++) {
			if (this[i] === obj) { return i; }
		}
		return -1;
	}

}
window.UTIL = {};

UTIL.wait = {
	/** nyni tri callbacky:
	 * prvni je volany vzdy pokud vsechny skonci, jako prni parametr dostane stav (successAll, errorAll, mixed)
	 * druhy se vola v pripade uspechu vsech, treti v pripade neuspechu vsechna  posledni v pripade kombinovanych stavu
	 */
	forAll: function(finishedCallback, successAllCallback, errorAllCallback, errorAnyCallback) {
		var state = 'notready', callCounter = 0, callbacks = [], lastPromise, finished = false;

		function isComplete() {
			return _(callbacks).all(function (item) {return item !== false;});
		}

		function getFinalState() {
			if (_(callbacks).all(function (item) {return item == "success";})) {
				return "successAll";
			} else if (_(callbacks).all(function (item) {return item == "error";})) {
				return "errorAll";
			} else {
				return "mixed";
			}
		}

		function finish() {
			state = getFinalState();
			finishedCallback(state);
			if (state == "successAll" && successAllCallback) {
				successAllCallback();
			}
			if (state == "errorAll" && errorAllCallback) {
				errorAllCallback();
			}
			if (state == "mixed" && errorAnyCallback) {
				errorAnyCallback();
			}
		}

		function createPromise() {
			var resolve, callId = callCounter++;
			callbacks[callId] = false;
			resolve = function (type) {
				callbacks[callId]  = type;
				if (isComplete()) {
					finish();
				}
			};

			return {
				/** callback pro uspech */
				success: function () {
					return resolve("success");
				},
				/** callback pro chybu */
				error: function() {
					return resolve("error");
				},

				/** v pripade ze by bylo nekdy potreba zjistit stav, konkr.
				 * promise */
				myStatus: function() {
					return callbacks[callId];
				}
			};
		}
		return {
			/*
			 * vyrobi objekt, ktery obsahuje metody success, error, ktere lze
			 * zaregistrovat jako listenery k JEDNE akci.
			 */
			wait: function() {
				lastPromise = createPromise();
				return lastPromise;
			},

			/** umoznuje se dostat k posledni vracene hodnote wait, bez vyvtoreni
			 * nove instance. Proste slouzi k tomu, aby nebylo treba vysledek wait
			 * nekam ukladat, kdyz chceme naplnit error i success callbacky*/
			lastWait: function() {
				return lastPromise;
			},

			start: function() {
				state = "waiting";
				if (isComplete()) {
					finish();
				}
			},

			getState: function() {
				return state;
			}
		};
	}
};


//rq. underscore
(function (global) {
	//variables
	var types = [ 'debug', 'info','log', 'warn', 'error', 'exception'],
		type2nativeMap = { "exception" : "error" },
		nativeConsole = global.console,
		logLevel = 'debug', logLevelIndex = types.indexOf(logLevel),
		logOn = true,
		log2Native = true,
		logParamsJoined = false,
		reducedLogMethods = true, //ve phonegapu (ios) umi do xcode poslat jen log, warn a error, ostatni zahazuje
		ios = navigator.userAgent.match(/(iphone|ipod|ipad)/i),
		android = navigator.userAgent.match(/android/i),
		maxStringifyNestingLevel = 2,
		logSource = false
		;
	logParamsJoined = (ios || android) ? true : false;
	reducedLogMethods = ios;

	function getErrorObject(){
		return (new Error());
	}

	function getSource() {
		var err = getErrorObject(),
			caller_line,
			index,
			clean;
		if (err.stack) {
			caller_line = err.stack.split("\n")[4];
			index = caller_line.indexOf("at ");
			clean = caller_line.slice(index+2, caller_line.length);
		}
		return [caller_line, index, clean];
	}

	//FN
	function formatException(e) {
		var str = "EXC:";
		if (_.isString(e)) {
			str += e;
		} else {
			if (e.name) {
				str += e.name + " ";
			}
			if (e.message || e.description) {
				str += (e.message || e.description) + " \n";
			}
			if (e.stack) {
				str += e.stack;
			}

		}

		return str;
	}


	function logNative(type, arg) {
		var nativeMethod;
		if (nativeConsole && log2Native) {
			//zjisti metodu logovadla
			nativeMethod = type;
			if (type in type2nativeMap) {
				nativeMethod = type2nativeMap[type];
			}

			//prepro - join?
			if (logParamsJoined) {
				arg = [arg.join(", ")];
			}

			//phoengap ios ma omezenou sadu metod, ktere sleduje
			if (reducedLogMethods && ["debug", "info"].indexOf(nativeMethod) !== -1) {
				nativeMethod = "log";
			}

			//log
			//nativeConsole[nativeMethod].apply(nativeConsole, arg);
			if (global.console && nativeMethod in global.console) {
				try {
					global.console[nativeMethod].apply(global.console, arg);
				} catch (e) { /* kvuli ie 8-*/ }
			}
		}
	}

	function logInternal(type) {
		var arg  = _.toArray(arguments),
			source;
		arg.splice(0,1);

		if (type == "exception") {
			arg = [formatException(arg[0])];
			type = "error";
		}

		if (logOn && logLevelIndex <= types.indexOf(type)) {
			logNative(type, arg);
		}

		if (logSource) {
			source = getSource();
			if (source && source.length) {
				logNative("log", [ source[0] ]);
			}
		}

	}


	function initInjectMethods(exportObj, types) {
		_(types).each(function (type) {
			exportObj[type] = _(logInternal).bind(exportObj, type);
		});
	}


	function stringify(o, simple, level) {
		var json = '', i, type = ({}).toString.call(o), parts = [], names = [];
		level = (level || 0) + 1;
		if (level > maxStringifyNestingLevel) {
			return "|nestedTooDeep|";
		}

		if (type == '[object String]') {
			json = '"' + o.replace(/\n/g, '\\n').replace(/"/g, '\\"') + '"';
		} else if (type == '[object Array]') {
			json = '[';
			for (i = 0; i < o.length; i++) {
				parts.push(stringify(o[i], simple, level));
			}
			json += parts.join(', ') + ']';
		} else if (type == '[object Object]') {
			json = '{';
			for (i in o) {
				names.push(i);
			}
			//names.sort(sortci);
			for (i = 0; i < names.length; i++) {
				parts.push(stringify(names[i]) + ': ' + stringify(o[names[i] ], simple, level));
			}
			json += parts.join(', ') + '}';
		} else if (type == '[object Number]') {
			json = o+'';
		} else if (type == '[object Boolean]') {
			json = o ? 'true' : 'false';
		} else if (type == '[object Function]') {
			json = o.toString();
		} else if (o === null) {
			json = 'null';
		} else if (o === undefined) {
			json = 'undefined';
		} else if (simple == undefined) {
			json = type + '{\n';
			for (i in o) {
				names.push(i);
			}
			names.sort(sortci);
			for (i = 0; i < names.length; i++) {
				parts.push(names[i] + ': ' + stringify(o[names[i]], true, level)); // safety from max stack
			}
			json += parts.join(',\n') + '\n}';
		} else {
			try {
				json = o+''; // should look like an object
			} catch (e) {}
		}
		return json;
	}



	global.L = {
		nativeOn: function () {
			log2Native = true;
		},

		nativeOff: function () {
			log2Native = false;
		},

		loggingOn: function () {
			logOn = true;
		},

		loggingOff: function () {
			logOn = false;
		},

		logSourceOn: function () {
			logSource = true;
		},

		logSourceOff: function () {
			logSource = false;
		},

		joinParamsOn: function () {
			logParamsJoined = true;
		},

		joinParamsOff: function () {
			logParamsJoined = false;
		},

		logLevel: function (level) {
			if (!_.isUndefined(level))  {
				logLevel = level;
				logLevelIndex = types.indexOf(level);
			}

			return logLevel;

		},

		obj: function (obj) {
			return stringify(obj, true);
		}
	};

	initInjectMethods(global.L, types);
}(window));





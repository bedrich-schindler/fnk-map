SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function (toElement) {
  return toElement.getScreenCTM().inverse().multiply(this.getScreenCTM());
};

/* global jQuery, Shops, UTIL */
(function ($) {
  /**
   * SVG implementace mapy.
   * @version 3.0.0
   */
  window.MapImplementationSvg = function (mapComponent) {
    this.component = mapComponent;
    this.$el = this.component.$el;
  };

  MapImplementationSvg.prototype = {
    component: null,
    canvas: null,
    fillAnimateDuration: 250,
    grayedShopColor: '#999',
    foundShopColor: 'red',
    requiresGroupedElements: false,
    $el: null,

    init: function () {
      this._loadSvg(this.$el.attr('data-map-image'));
      this.grayedShopColor = Map.grayedShopColor;
      this.foundShopColor = Map.foundShopColor;
    },

    activate: function () {
      this.$el.on('click', '.shop', _(this.component.onShopClick).bind(this.component));

      if (Map.maptype === Map.MAP_TYPE_HOVER) {
        this.$el.on('mouseover', '.shop', _(this.component.onShopHover).bind(this.component));
      }
    },

    deactivate: function () {
      this.$el.off('click', '.shop');

      if (Map.maptype === 'hover') {
        this.$el.off('mouseover', '.shop');
      }

      this.resetColors();
    },

    getMapIdFromElm: function (shopEl) {
      return shopEl.attr('id');
    },

    _loadSvg: function (url) {
      var that = this;
      var imageSize = this.component.getRealMapSize();

      this.$el.svg({
        onLoad: function () {
          that.canvas = that.$el.svg('get');
          that.canvas.load(url, {
            onLoad: _(that.onLoadSvg).bind(that),
            changeSize: false
          });
        },
        settings: {width: imageSize[0], height: imageSize[1]}
      });
    },

    onLoadSvg: function () {
      this.loaded = true;
      this.initPois();

      this.component.onLoad();

      var svgId = this.$el.find('svg')[0].id;
      var svg = d3.select('#' + svgId);

      // Enables zoom and pan
      
      svg
        .call(
          d3.zoom()
            .scaleExtent([0.5, 4])
            .on('zoom', function () {
              d3.select('#' + svgId + '-view')
                .attr('transform', d3.event.transform);
            })
        );

      // Load shop labels

      Map.shops.forEach(function (shop) {
        shop.mapNumber.forEach(function(mapNumber) {
          d3.selectAll('#shop' + mapNumber + ' > *').text(shop.name);
        });
      });
    },

    initPois: function () {
      this.$el.find('.poi').hide();
    },

    getShopElementById: function (shop) {
      if (!_.isArray(shop)) {
        shop = Map.getShopMapIds(shop);
      }

      var canvas = this.canvas;
      var found = null;

      _(shop).each(function (item) {
        if (!found) {
          found = canvas.root().getElementById(item);
        }
      });

      return found;
    },

    markShop: function (shop, callback) {
      var shopEl = this.getShopElementById(shop);

      if (shopEl) {
        this.grayoutExcept([shop], function () {
          if (callback) {
            callback();
          }
        }, this.fillAnimateDuration);
      }
    },

    grayoutExcept: function (shops, complete, duration) {
      var that = this;
      var mapIds = [];

      _(shops).each(function (item) {
        mapIds = _.union(mapIds, Map.getShopMapIds(item));
        var itemElm = that.getShopElementById(item);
      });

      var waiter = UTIL.wait.forAll(function () {
        if (_.isFunction(complete)) {
          complete();
        }
      });

      this._colored = true;
      this._stopAnimations();

      this.$el.find('.shop').each(function () {
        var elm = $(this);
        var mapId = elm.attr('id');
        var shopId = null;

        if (mapId.startsWith('shop')) {
          shopId = parseInt(mapId.replace('shop', ''), 10);
        }

        if (Map.foundShopIds.indexOf(shopId) !== -1) {
          that._colorShopElement(elm, that.foundShopColor, 0, waiter.wait().success);
        } else if (!_(mapIds).contains(mapId)) {
          that._colorShopElement(elm, that.grayedShopColor, duration, waiter.wait().success);
        } else if (Map.selectedShopColor) {
          //barvime obchod na nastavenou barvu
          that._colorShopElement(elm, Map.selectedShopColor, duration, waiter.wait().success);
        } else {
          //vracime barvu obchodu na jeho vychozi (oldfill)
          that._resetShopElementColor(elm, duration, waiter.wait().success);
        }
      });

      waiter.start();
    },

    colorShop: function (mapId, complete, duration) {
      this._colored = true;
      this._stopAnimations();
      this._resetShopElementColor($(mapId), duration, complete);
    },

    colorShops: function (shops, complete, duration) {
      var mapIds = _(shops).map(function (item) {
        return item.getMapId();
      });
      var waiter = UTIL.wait.forAll(complete || function () {});

      this._colored = true;
      this._stopAnimations();

      _(mapIds).each(function (mapId) {
        this._resetShopElementColor($(mapId), duration, waiter.wait().success);
      }, this);

      waiter.start();
    },

    colorAllShops: function (complete) {
      this._colorAllShops(complete);
    },

    _stopAnimations: function () {
      $(this.canvas.root()).stop(true, true);
      this.$el.find('.shop').find(':not(g)').andSelf().stop(true, true);
    },

    _colorAllShops: function (complete) {
      var that = this;
      this._stopAnimations();
      this._colored = false;

      this.$el.find('.shop').each(function () {
        var shopEl = $(this);
        that._resetShopElementColor(shopEl);
      });

      if (_.isFunction(complete)) {
        _.defer(complete);
      }
    },

    resetColors: function () {
      if (this._colored) {
        if (Map.Timer.isPlannedTimeout()) {
          Map.Timer.removePlannedTimeout();
        }

        this._colored = false;
        this.colorAllShops();
      }
    },

    _colorShopElement: function (elm, color, duration, complete) {
      if (elm.is('g')) {
        var colorableChildren = elm.children().not('[class=logo]').not('[class=pictogram]').not('[class=nofill]').not('g');
      } else {
        if (false && this.requiresGroupedElements) {
          return null;
        } else {
          colorableChildren = $(elm);
        }
      }

      //store old color
      colorableChildren.each(function (index, elm) {
        var $elm = $(elm);

        if (!$elm.attr('colored')) {
          if ($elm.attr('fill')) {
            $elm.attr('oldfill', $elm.attr('fill'));
          }

          $elm.attr('colored', '1');
        }
      });

      if (duration) {
        $.when(colorableChildren.animate({'svgFill': color}, {duration: duration})).then(complete);
      } else {
        colorableChildren.attr('fill', color);

        if (_.isFunction(complete)) {
          _(complete).defer();
        }
      }
    },

    _resetShopElementColor: function (elm, duration, complete) {
      var waiter = UTIL.wait.forAll(complete || function () {});
      var colorableChildren = null;

      if (elm.is('g')) {
        colorableChildren = elm.find(':not(g)');

      } else {
        if (false && this.requiresGroupedElements) {
          return null;
        } else {
          colorableChildren = $(elm);
        }
      }

      //store old color
      colorableChildren.each(function (index, elm) {
        var $elm = $(elm);

        if ($elm.attr('colored')) {
          var color = $elm.attr('oldfill') ? $elm.attr('oldfill') : false;

          if (duration && color) {
            $elm.animate({'svgFill': color}, {duration: duration, complete: waiter.wait().success});
          } else {
            if (color) {
              $elm.attr('fill', color);
            } else {
              $elm.removeAttr('fill');
            }
          }
        }
      });
      if (!duration) {
        if (_.isFunction(complete)) {
          _(complete).defer();
        }
      } else {
        waiter.start();
      }
    },

    fit: function () {
      //nafituje dle rozmeru rodice;
      var w = this.component._normalWidth;
      var h = this.component._normalHeight;
      this.$el.height(h);
      this.$el.width(w);
      $(this.canvas.root()).attr('width', w + 'px').attr('height', h + 'px');
    },

    reset: function () {
      this.resetColors();
    },

    hide: function (after) {
      $(this.canvas.root()).animate({
        'opacity': 0
      }, 250, 'linear', function () {
        if (_.isFunction(after)) {
          after();
        }
      });
    },

    show: function (after) {
      $(this.canvas.root()).animate({
        'opacity': 1
      }, 250, 'linear', function () {
        if (_.isFunction(after)) {
          after();
        }
      });
    },

    showPois: function (type) {
      this.$el.find('.poi' + type).show();
    },

    hidePois: function () {
      this.$el.find('.poi').hide();
    }
  };

  /**
   * @param el
   * @param initCallback - fce, param this
   * @constructor
   *
   * Rozhrani pro jedno patro mapy.
   */
  window.MapComponent = function (el, initCallback) {
    this.$el = $(el);
    this.initCallback = initCallback;
    this.init();
  };

  MapComponent.prototype = {
    $el: null,
    initCallback: null,
    loaded: false,
    active: false,
    loaded: false,

    impl: null,

    showShopId: null,

    _normalWidth: null,
    _normalHeight: null,

    _markedShopId: null,

    _level: null,


    init: function () {
      this._level = parseInt(this.$el.attr('data-map-level'), 10);
      this.updateSize(this.getRealMapSize());

      this.impl = new MapImplementationSvg(this);
      this.impl.init();
    },

    updateSize: function (size) {
      this._normalWidth = size[0];
      this._normalHeight = size[1];
    },

    onLoad: function () {
      this.loaded = true;
      this.fit();

      if (this.initCallback) {
        this.initCallback(this);
      }

      if (this.showShopId) {
        var shop = Map.findShopById(this.showShopId);
        this.showShopId = null;

        if (shop) {
          this.markShop(shop);
        }
      }
    },

    activate: function () {
      this.active = true;
      this.impl.activate();
    },

    deactivate: function () {
      this.active = false;
      this.impl.deactivate();
    },

    onShopHover: function (e) {
      var shopEl = $(e.target).closest('.shop').first();
      var mapId = this.impl.getMapIdFromElm(shopEl);
      var shop;

      e.preventDefault();
      e.stopPropagation();

      shop = Map.findShopByMapId(mapId);

      if (Map.Timer.isPlannedTimeout()) {
        Map.Timer.removePlannedTimeout();
      }

      if (shop && this._markedShopId == shop.id) {
        //nothing
      } else {
        this.markShop(shop);
      }

      return false;
    },
    onShopClick: function (e) {
      var shopEl = $(e.target).closest('.shop').first();
      var mapId = this.impl.getMapIdFromElm(shopEl);
      var shop;

      e.preventDefault();
      e.stopPropagation();

      shop = Map.findShopByMapId(mapId);

      if (shop && this._markedShopId == shop.id) {
        Map.showShopDetail(shop);
      } else {
        this.markShop(shop);
      }

      return false;
    },

    markShop: function (shop, callback) {
      if (shop) {
        this._markedShopId = shop.id;
        this.impl.markShop(shop, callback);
      }

      return null;
    },

    moveInto: function (targetParent) {
      targetParent.append(this.$el);
      this.fit();
    },

    hide: function (after) {
      this.deactivate();
      this.impl.hide(after);
    },

    show: function (after) {
      this.fit();
      this.impl.show(after);
    },

    /**
     * Velikost jakou ma/bude mapa zobrazena.
     * @return [sirka, vyska]
     */
    getRealMapSize: function () {
      var imgHeight = this.$el.attr('data-display-height');
      var imgWidth = this.$el.attr('data-display-width');

      if (Map.fitMapToHeight) {
        var imgRatio = imgWidth / imgHeight;
        var targetWidth = Math.round(imgRatio * Map.fitMapToHeight);

        return [targetWidth, Map.fitMapToHeight];
      } else {
        return [imgWidth, imgHeight];
      }
    },

    fit: function () {
      var imageSize = this.getRealMapSize();

      //updatni velikost containeru, pokud si ji mapa s sebou nese
      if (this.active && Map.fitContainerSizeToMap) {
        this.$el.parent()
          .height(imageSize[1])
          .width(imageSize[0]);
      }

      //aktualizuj rozme rodice podle tebe
      if (this.active && Map.fitMapToHeight) {
        this.$el.parent().height(imageSize[1]);
        //tohle asi neni uplne ideal, ale potrebuju iscrolovaci mapu vycentrovat i v pripadde zeiscroll neni potreba
        var parentWidth = imageSize[0];

        if (parentWidth < this.$el.parent().parent().width()) {
          parentWidth = this.$el.parent().parent().width();
        }

        this.$el.parent().width(parentWidth);
        this.$el.parent().height(Map.fitMapToHeight);
      }

      this.updateSize(imageSize);
      if (this.loaded) {
        this.impl.fit();
      }

      if (this.loaded && this.active) {
        _.defer(_(function () {
          //hotfix jednoho problemu s webkit android, kdy bez defer
          //jeste iscroll neumel zmerit spravne sirku elementu, kterou nastavujeme vyse...
          //ve style byla, ale dom to asi nejak nebyl schopnej vratit
          this.centerMap();
        }).bind(this));
      }
    },

    centerMap: function () {
      if (this.$el.parent().parent().width() < this.$el.parent().width()) {
        var xoffset = -this.$el.parent().width() / 2 + this.$el.parent().parent().width() / 2;

        if ($(Map.mapScrollWrapperSelector).length) {
          $(Map.mapScrollWrapperSelector)[0].scrollLeft = -xoffset;
        }
      }
    },

    reset: function () {
      this.impl.reset();
    },

    showPois: function (type) {
      this.impl.showPois(type);
    },

    hidePois: function () {
      this.impl.hidePois();
    }
  };


  /**
   * @namespace Map
   * @name Map
   *
   * Inicializuj volanim Map.init();
   * Lze konfigurovat
   * //povinne
   * Map.shops = []; //json obchodu (drive v MapShopData);
   * Map.pageBlockSelector = ''; selector bloku stranky
   * Map.currentLevel = ''; oznaceni patra ktere ma byt ve vychozim stavu zobrazeno
   *
   * //nepovinne
   * Map.grayedShopColor
   * Map.foundShopColor // Barva vyhledaneho obchodu
   * Map.showShopId = 89; //id obchodu, ktery ma byt po zobrazeni mapy zobrazen.
   * Map.mapContainerSelector = '..'; //selektor pro mapContaienr. Vychozi '.map-container'
   * Map.maptype = 'hover'; //pokud ma reagovat na hover
   * Map.wrapFallbackMapInMapElement = true; //pokud ma ziskanou klikaci mapu zabalit do <map> elementu. Vyhozi hodnota (false) znamena, ze pocita s <map>lementem v datech.
   * Map.fitContainerSizeToMap = false; // zda si ma mapa prizpusobovat velikost kontaineru a ne naopak.
   * Map.fitMapToHeight = 300; rika mape, ze ma pevnou vysku a do te se ma fitovat s tim, ze sirka pak bude ruzna
   *
   */
  window.Map = {
    /** @lends Map */
    /**
     * Hotnoty pro maptype
     * Mapa reaguje na hover
     */
    MAP_TYPE_HOVER: 'hover',
    /**
     * Hotnoty pro maptype
     * Mapa reaguje az na click
     */
    MAP_TYPE_CLICK: 'click',
    /**
     * Hotnoty pro switchMode
     * Mapa se prepina pres miniatury mapy
     */
    MAP_SWITCH_MODE_ICO: 'mapico',
    /**
     * Hotnoty pro switchMode
     * Mapa se prepina pres seznam pater
     */
    MAP_SWITCH_MODE_LIST: 'list',

    apiUrl: 'http://forumnovakarolina.dev.smarcoms.cz/cs',
    apiShopListPath: '/mapa',
    apiShopDetailPath: '/mapa?shopid=:id',
    apiCategoryListPath: '/mapa-seznam',
    apiCategoryDetailPath: '/mapa-kategorie/:id',

    shops: null,
    currentLevel: null,
    showShopId: null,
    foundShopIds: [],
    wrapFallbackMapInMapElement: false,
    forceFallback: false,
    mapContainerSelector: '.map-container',
    mapScrollWrapperSelector: '.map-wrapper',
    mapCategoryContainerSelector: '.map-category-container',
    pageBlockSelector: null,
    maptype: 'hover',
    switchMode: 'mapico',
    icoSwitchSelector: '.map-ico > a',
    listSwitchSelector: 'ul.map-nav li',
    fitContainerSizeToMap: true,
    fitMapToHeight: null,
    onLevelChanged: null,
    grayedShopColor: '#999',
    foundShopColor: 'red',
    selectedShopColor: null,

    /** zavola se po inicializaci levelu, callbak je function(lvl, component) kde lvl je patro acomponent je kompoenta par*/
    onLevelInit: null,

    shopSearchSelector: '.map-shop-search-input',

    shopDetailSelector: '.map-shop-detail',
    shopDetailCloseSelector: '.map-shop-detail-close',
    shopDetailTitleSelector: '.map-shop-detail-title',
    shopDetailOpeningHoursSelector: '.map-shop-detail-opening-hours',

    levelMaps: {},
    $pageBlock: null,
    animationInProgress: false,

    Timer: {
      timeout: 100,

      _timeoutEvent: null,
      _timeoutIsPlanned: false,
      _timeoutCallback: null,

      isPlannedTimeout: function () {
        return this._timeoutIsPlanned;
      },

      planTimeout: function (callback) {
        this._timeoutIsPlanned = true;
        this._timeoutCallback = callback;
        this._timeoutEvent = setTimeout(function () {
          Map.Timer.runPlannedCallback();
        }, this.timeout);
      },

      removePlannedTimeout: function () {
        clearTimeout(this._timeoutEvent);
        this._timeoutIsPlanned = false;
      },

      runPlannedCallback: function () {
        this._timeoutIsPlanned = false;
        this._timeoutEvent = null;

        return $.isFunction(this._timeoutCallback) && this._timeoutCallback();
      }
    },

    transformShopData: function (shop) {
      return {
        id: shop.nid[0].value,
        mapNumber: shop.field_cislo_obchodu.map(function (mapNumber) {
          return mapNumber.value;
        }),
        name: shop.title [0].value,
        level: shop.field_podlazi.map(function (level) {
          return level.value;
        }),
        openingHours: shop.field_oteviraci_doba[0].value,
      };
    },

    transformCategoryData: function (category) {
      return {
        id: category.tid[0].value,
        name: category.name[0].value
      };
    },

    init: function (onLoad) {
      var that = this;

      this.initShopList(function () {
        that.initCategoryList();
        that.initShopSearch();

        that.$pageBlock = $(that.pageBlockSelector).last();
        //loadni jednotlive mapy.
        that.$pageBlock.find(that.mapContainerSelector).each(function () {
          var mapEl = $(this);
          Map.initMapLevel(mapEl);
        });

        //osetri klik na zmenu patra
        that.initLevelToggle();
        //kliknuti mimo klikaci veci
        that.$pageBlock.on('click', _(that.onAnythingClick).bind(that));

        if (Map.maptype == Map.MAP_TYPE_HOVER) {
          that.$pageBlock.find(that.mapContainerSelector).on('mouseleave', _(that.onAnythingHover).bind(that));
        }

        onLoad();
      });
    },

    initShopList: function(onLoad) {
      var that = this;

      $.getJSON(that.apiUrl + that.apiShopListPath)
        .done(function (jsonData) {
          Map.shops = jsonData.map(that.transformShopData);
          onLoad();
        });
    },

    initCategoryList: function () {
      var that = this;

      $.getJSON(that.apiUrl + that.apiCategoryListPath)
        .done(function (jsonData) {
          jsonData
            .map(that.transformCategoryData)
            .forEach(function (category) {
              var $elCategory = $('<button></button>')
                .text(category.name)
                .attr('data-map-category-id', category.id);

              $(that.mapCategoryContainerSelector).append($elCategory);
            });

          that.initCategoryListSearch();
        });
    },

    initCategoryListSearch: function() {
      var that = this;

      $('[data-map-category-id]').click(function () {
        var $el = $(this);
        var foundShopIds = [];
        var categoryId = $(this).attr('data-map-category-id');

        $('.map-shop-search-input').val('');

        if ($el.hasClass('active')) {
          d3.selectAll('.shop > *')
            .attr('oldfill', that.grayedShopColor)
            .attr('fill', that.grayedShopColor);
          $el.removeClass('active');

          that.foundShopIds = [];

          return;
        }

        $.getJSON(that.apiUrl + that.apiCategoryDetailPath.replace(':id', categoryId), function (dataJson) {
          var shops = dataJson.map(that.transformShopData);
          var foundShopIds = [];
          var anyShopOnActiveLevel = false;

          d3.selectAll('.shop > *')
            .attr('oldfill', that.grayedShopColor)
            .attr('fill', that.grayedShopColor);

          shops.forEach(function (shop) {
            if (shop.level.indexOf(that.currentLevel) !== -1) {
              anyShopOnActiveLevel = true;
            }

            shop.mapNumber.forEach(function (mapNumber) {
              foundShopIds.push(mapNumber);

              d3.selectAll('#shop' + mapNumber + ' > *')
                .attr('oldfill', that.foundShopColor)
                .attr('fill', that.foundShopColor);
            });
          });

          if (!anyShopOnActiveLevel && shops.length > 0) {
            $('[data-map-level=' + shops[0].level[0] + ']').trigger('click');
          }

          that.foundShopIds = foundShopIds;

          $('[data-map-category-id]').removeClass('active');
          $el.addClass('active');
        });
      });
    },

    initShopSearch: function () {
      var that = this;

      $(that.shopSearchSelector).easyAutocomplete({
        url: that.apiUrl + that.apiShopListPath,

        getValue: function (data) {
          return data.title[0].value;
        },

        list: {
          onSelectItemEvent: function () {
            var foundShopIds = [];
            var $el = $(that.shopSearchSelector);
            var selectedShop = $el.getSelectedItemData();
            var shop = that.transformShopData(selectedShop);

            shop.mapNumber.forEach(function (mapNumber) {
              foundShopIds.push(mapNumber);

              d3.selectAll('.shop > *')
                .attr('oldfill', that.grayedShopColor)
                .attr('fill', that.grayedShopColor);
              d3.selectAll('#shop' + mapNumber + ' > *')
                .attr('oldfill', that.foundShopColor)
                .attr('fill', that.foundShopColor);
            });

            if (shop.level.indexOf(that.currentLevel) === -1) {
              $('[data-map-level=' + shop.level[0] + ']').trigger('click');
            }

            $('[data-map-category-id]').removeClass('active');

            Map.foundShopIds = foundShopIds;
          },

          onHideListEvent: function () {
            var $el = $(that.shopSearchSelector);
            var value = $el.val();

            if (value === '') {
              Map.foundShopIds = [];

              d3.selectAll('.shop > *')
                .attr('oldfill', that.grayedShopColor)
                .attr('fill', that.grayedShopColor);
            }
          },

          match: {
            enabled: true
          },
        },

        template: {
          type: 'description',
          fields: {
            description: function (data) {
              return that.transformShopData(data).level.map(function (level) {
                return level + '. patro';
              }).join(',');
            }
          }
        },

        theme: 'square'
      });
    },

    initMapLevel: function (mapEl) {
      var level = parseInt(mapEl.attr('data-map-level'), 10);
      var map = Map.levelMaps[level] = new MapComponent(mapEl, function (component) {
        if (Map.onLevelInit) {
          Map.onLevelInit(level, component);
        }
      });
      map.level = level;

      if (level === this.currentLevel) {
        map.showShopId = this.showShopId;

        map.updateSize(map.getRealMapSize());
        map.activate();
      }
    },

    initLevelToggle: function () {
      if (this.switchMode == Map.MAP_SWITCH_MODE_ICO) {
        this.$pageBlock.on('click', this.icoSwitchSelector, _(this.onIcoLevelChangeClick).bind(this));
      } else {
        var level = this.currentLevel;
        $(Map.listSwitchSelector).click(function (e) {
          e.preventDefault();
          level = parseInt($(this).attr('data-map-level'), 10);
          Map.showLevel(level);
          $(Map.listSwitchSelector).removeClass('active');
          $(this).addClass('active');

          return false;
        });
      }
    },

    onIcoLevelChangeClick: function (e) {
      var t = $(e.target).closest('a');
      var newLevel = parseInt(t.find('.map-container').attr('data-map-level'), 10);

      e.preventDefault();
      e.stopPropagation();

      if (newLevel !== this.currentLevel && !this.animationInProgress) {
        this.animationInProgress = true;
        this.swapMaps(newLevel);
      }
    },

    /** preprinani seznamem pater */
    showLevel: function (newLevel, callback) {
      if (newLevel !== this.currentLevel) {
        var newMap = this.levelMaps[newLevel];
        var oldMap = this.levelMaps[this.currentLevel];

        oldMap.deactivate();
        oldMap.hide(_(this._showLevelWhenHidden).bind(this, newLevel, newMap, oldMap, callback));
      }
    },

    /** preprinani seznamem pater */
    _showLevelWhenHidden: function (newLevel, newMap, oldMap, callback) {
      var newEl = this.levelMaps[newLevel].$el;
      var oldEl = this.levelMaps[this.currentLevel].$el;

      oldEl.hide();
      newMap.activate();
      newEl.show();
      newMap.fit();

      if (this.onLevelChanged) {
        this.onLevelChanged(newLevel);
      }

      newMap.show(_(this._showLevelWhenShown).bind(this, newLevel, newMap, oldMap, callback))
    },

    _showLevelWhenShown: function (newLevel, newMap, oldMap, callback) {
      this.currentLevel = newLevel;
      this.animationInProgress = false;

      if (callback) {
        callback();
      }
    },


    /** preprinani ikonkami s miniaturou pater */
    swapMaps: function (newLevel, callback) {
      //skyr zobrazenou a presun
      var newMap = this.levelMaps[newLevel];
      var oldMap = this.levelMaps[this.currentLevel];
      var waitForHide = UTIL.wait.forAll(_(this._swapMapsWhenHidden).bind(this, newLevel, newMap, oldMap, callback));

      newMap.hide(waitForHide.wait().success);
      oldMap.hide(waitForHide.wait().success);
      waitForHide.start();
    },

    /** preprinani ikonkami s miniaturou pater */
    _swapMapsWhenHidden: function (newLevel, newMap, oldMap, callback) {
      var targetSmall = this.levelMaps[newLevel].$el.parent();
      var targetBig = this.levelMaps[this.currentLevel].$el.parent();

      //updatni velikost containeru, pokud si ji mapa s sebou nese
      //tohle jsem z nejakyho duvodu dal pryc
//            this.fitMapParentIfShould(newMap, this.levelMaps[this.currentLevel].$el);

      oldMap.deactivate();
      newMap.activate();
      newMap.moveInto(targetBig);
      oldMap.moveInto(targetSmall);

      //nazvy pater
      targetBig.closest('.center-map').find('h2').text(newMap.$el.attr('data-map-label'));
      targetSmall.closest('a').find('.map-label').text(oldMap.$el.attr('data-map-label'));

      if (this.onLevelChanged) {
        this.onLevelChanged(newLevel);
      }

      var waitForShow = UTIL.wait.forAll(_(this._swapMapsWhenShown).bind(this, newLevel, newMap, oldMap, callback));

      newMap.show(waitForShow.wait().success);
      oldMap.show(waitForShow.wait().success);
    },

    /** preprinani ikonkami s miniaturou pater */
    _swapMapsWhenShown: function (newLevel, newMap, oldMap, callback) {
      this.currentLevel = newLevel;
      this.animationInProgress = false;

      if (callback) {
        callback();
      }
    },

    onAnythingClick: function (e) {
      var t = $(e.target);

      if (!t.is('a') && !t.is('input')) {
        if (!Map.Timer.isPlannedTimeout()) {
          Map.Timer.planTimeout(function () {
            Map.levelMaps[Map.currentLevel].reset();
          });
        }
      }
    },

    onAnythingHover: function (e) {
      var t = $(e.target);

      if (!Map.Timer.isPlannedTimeout()) {
        Map.Timer.planTimeout(function () {
          Map.levelMaps[Map.currentLevel].reset();
        });
      }
    },

    findShopByMapId: function (mapId) {
      var that = this;

      return _(this.shops).find(function (item) {
        var shopMapIds = that.getShopMapIds(item);
        var contain = _(shopMapIds).contains(mapId);

        return contain && _(item.level).contains(Map.currentLevel);
      });
    },

    getShopMapIds: function (shop) {
      if (!('mapIds' in shop)) {
        shop.mapIds = _(shop.mapNumber).map(function (item) {
          return 'shop' + $.trim(item)
        });
      }

      return shop.mapIds;
    },


    findShopById: function (id) {
      return _(this.shops).find(function (item) {
        return $.trim(item.id) == $.trim(id);
      });
    },

    showPois: function (type) {
      this.levelMaps[this.currentLevel].showPois(type);
    },

    hidePois: function () {
      this.levelMaps[this.currentLevel].hidePois();
    },

    /**
     * Oznaci obchod na mape dle id, postara se o zmenu patra.
     * Pokud ch
     * @param id -id obchodu
     * @param [forceLevel=null] pokud chceme vynutit patro, tak patro
     */
    markShopById: function (id, forceLevel) {
      var level, shop, afterFn;
      shop = this.findShopById(id);

      if (!shop) {
        return;
      }

      this.markingShop = true;

      if (forceLevel) {
        level = forceLevel;
      } else {
        level = shop.level[0];
      }

      afterFn = _(this.markShopWhenFloorShown).bind(this, shop);

      if (this.currentLevel !== level) {
        //prepneme si patro
        if (this.switchMode == Map.MAP_SWITCH_MODE_ICO) {
          this.swapMaps(level, afterFn);
        } else {
          this.showLevel(level, afterFn)
        }
      } else {
        afterFn();
      }
    },

    markShopWhenFloorShown: function (shop) {
      this.levelMaps[this.currentLevel].markShop(shop, function () {
        Map.markingShop = false;
      });
    },

    showShopDetail: function (shop) {
      var that = this;

      $.getJSON(this.apiUrl + this.apiShopDetailPath.replace(':id', shop.mapNumber[0]))
        .done(function (json) {
          if (!json.length) {
            return;
          }

          var shop = that.transformShopData(json[0]);

          $(that.shopDetailCloseSelector).click(function () {
            $(that.shopDetailSelector).hide();
          });

          $(that.shopDetailTitleSelector).text(shop.name);
          $(that.shopDetailOpeningHoursSelector).text(shop.openingHours);
          $(that.shopDetailSelector).show();
        });
    }
  };
})(jQuery);
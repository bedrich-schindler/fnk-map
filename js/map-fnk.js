// konfigurace mapy je v sablone
$(function () {
  function switchPoiIcons(level) {
    //zobraz/schovej ikonky poi, ktere jsou k dispozici v sablone
    var levelComponent = Map.levelMaps[level];

    if (levelComponent.$el.attr('data-available-poi-types')) {
      var poiTypes = levelComponent.$el.attr('data-available-poi-types').split(',');

      $('.map-icos > span').each(function () {
        var $el = $(this);

        if (poiTypes.indexOf($el.attr('data-poi')) !== -1) {
          $el.show();
        } else {
          $el.hide();
        }
      });
    }
  }

  function clearColorables($mapEl) {
    $mapEl.find('.colorable')
      .each(function () {
        var el = $(this);
        el.attr('data-colorable-fill', el.attr('fill'));
        el.attr('fill', 'transparent');
      })
      .on('mouseenter', function () {
        var el = $(this);
        el.attr('fill', el.attr('data-colorable-fill'));
      })
      .on('mouseleave', function () {
        var el = $(this);
        el.attr('fill', 'transparent');
      });
  }

  Map.onLevelChanged = function (level) {
    //zmen cislo dole
    $('.map-info .floor span').html(level);
    switchPoiIcons(parseInt(level, 10));
  };

  Map.onLevelInit = function (level, levelCmp) {
    clearColorables(levelCmp.impl.$el);
  };

  Map.init(function() {
    switchPoiIcons(Map.currentLevel);
  });


  $('.map-icos span')
    .mouseenter(function () {
      var type = $(this).attr('data-poi');
      Map.showPois(type);
    })
    .mouseleave(function () {
      Map.hidePois();
    });

  $('select.map-select').selectBoxIt({
    autoWidth: false
  });
  $('select.map-select').change(function () {
    Map.showLevel(parseInt($(this).val(), 10));
  });

});